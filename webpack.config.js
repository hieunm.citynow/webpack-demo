var webpack = require('webpack')
var path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// const ExtractTextPlugin = require("extract-text-webpack-plugin")  // TODO: 
// const PurifyCSSPlugin = require('purifycss-webpack')              // TODO: 
var CompressionPlugin = require('compression-webpack-plugin');

const config = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
      rules: [
          {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: 'babel-loader'
          },
          {
            test: /\.css$/,
            use: ['style-loader','css-loader']
          }
      ]
  },
  plugins: [
    new HtmlWebpackPlugin({template: './dist/index.html'}),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    // new webpack.optimize.DedupePlugin(), //dedupe similar code 
    // new webpack.optimize.AggressiveMergingPlugin(), //Merge chunks 
    new webpack.optimize.UglifyJsPlugin({
      // minimize: true,
      compress: { warnings: false },
      mangle: true,
      sourcemap: false,
      beautify: false,
      dead_code: true
    }), 
    // new webpack.HotModuleReplacementPlugin()
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
      threshold: 10240,
      minRatio: 0.8
    })
  ],
  devServer: {
    // contentBase: path.join(__dirname, "dist"),
    // compress: true,
    // port: 8080,
    // hot: true,
    // inline: true,
    clientLogLevel: "none"
  }
};

module.exports = config