
import { combineReducers } from 'redux';
import cart from './cart';
import product from './product';
import { reducer as toastrReducer } from 'react-redux-toastr'

const reducer = combineReducers({
    product,
    cart,
    toastr: toastrReducer
  })
  
  export default reducer