import React, { Component } from 'react';
import BrandDetail from './BrandDetail';
import brands from '../data/brands' // TODO: getBrandList
import { Link, Switch, Route } from 'react-router-dom'

const BrandList = () => (
    <div className="container-fluid">
        <Link to='/'>
            <h3>Back to Home</h3>
        </Link>
      {brands.map(b => (
        <li key={b.id}>
          <Link to={`/brands/${b.name}`}>{b.name}</Link>
        </li>))}
    </div>
  )

const Brands = () => (
    <main>
        <Switch>
            <Route exact path='/brands' component={BrandList}/>
            <Route path='/brands/:brandName' component={BrandDetail}/>
        </Switch>
    </main>
)

export default Brands;
