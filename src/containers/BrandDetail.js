import React, { Component } from 'react'
import '../styles/styles.css';
import ProductList from '../components/ProductList'
import { connect } from 'react-redux'
import { fetchProductsWithRedux, addToCart } from '../actions';
import { bindActionCreators } from 'redux'
import { Link } from 'react-router-dom'

class BrandDetail extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
      this.props.fetchProductsWithRedux();
    }

    render() {
        const { match: { params } } = this.props;
        return (
            <div className="Product-container container-fluid">
                <Link to='/brands'>
                    <h3>Back</h3>
                </Link>
                <h3>{params.brandName}</h3>
                { this.props.isFetching ? <img className="loading" src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" /> : 
                <div className="row">
                    <ProductList
                        products={this.props.products.filter((product) => product.brand == params.brandName)}/>
                </div> }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.product.products,
        isFetching: state.product.isFetching
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchProductsWithRedux: fetchProductsWithRedux
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(BrandDetail);