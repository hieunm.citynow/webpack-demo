import React, { Component } from 'react';
import '../styles/App.css';
import CartContainer from './CartContainer';
import Header from '../components/Header';
import Main from './Main';

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <Header />
        <Main />
        {/* Cart Modal */}
        <div className="container">
          <div className="modal fade" id="myModal" role="dialog">
            <div className="modal-dialog">
            
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                  <h4 className="modal-title">Shopping Cart</h4>
                </div>
                <div className="modal-body">
                  <CartContainer />
                </div>
              </div>
            </div>
          </div>
      </div>
      </div>
    );
  }
}

export default App;
