import React, { Component } from 'react';
import ProductsContainer from './ProductsContainer';
import Brands from './Brands';

class Home extends Component {
  render() {
    return (
      <div className="container-fluid">
        <ProductsContainer />
        <Brands />
      </div>
    );
  }
}

export default Home;
