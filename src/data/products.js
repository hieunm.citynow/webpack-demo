const data = [
    { "id": 0, "name": "WARD HI HIGH-TOP SNEAKER", "image": "https://dsw.scene7.com/is/image/DSWShoes/393645_001_ss_01?$pdp-image$", "brand": "Van", "price": 59.99, "inventory": 1 },
    { "id": 1, "name": "WARD LO SNEAKER", "image": "https://dsw.scene7.com/is/image/DSWShoes/405307_001_ss_01?$pdp-image$", "brand": "Van", "price": 54.99, "inventory": 8 }, 
    { "id": 2, "name": "FLEX EXPERIENCE RUN 6 LIGHTWEIGHT RUNNING SHOE", "image": "https://dsw.scene7.com/is/image/DSWShoes/377442_001_ss_01?$pdp-image$", "brand": "Nike", "price": 65.00, "inventory": 5},
    { "id": 3, "name": "CHUCK TAYLOR ALL STAR HIGH-TOP SNEAKER", "image": "https://dsw.scene7.com/is/image/DSWShoes/68596_000_ss_01?$pdp-image$", "brand": "Converse", "price": 54.99, "inventory": 3},
    { "id": 4, "name": "MSRMC GB HIGH-TOP SNEAKER", "image": "https://dsw.scene7.com/is/image/DSWShoes/400781_004_ss_01?$pdp-image$", "brand": "New Balance", "price": 84.99, "inventory": 8},
    { "id": 5, "name": "410 V5 TRAIL RUNNING SHOE", "image": "https://dsw.scene7.com/is/image/DSWShoes/389402_001_ss_01?$pdp-image$", "brand": "New Balance", "price": 59.99, "inventory": 2},
    { "id": 6, "name": "ON CLOUDVENTURE RUNNING SHOE", "image": "https://dsw.scene7.com/is/image/DSWShoes/407024_872_ss_01?$pdp-image$", "brand": "ON", "price": 149.99, "inventory": 8},
    { "id": 7, "name": "LAUNCH 4 LIGHTWEIGHT RUNNING SHOE", "image": "https://dsw.scene7.com/is/image/DSWShoes/399842_066_ss_01?$pdp-image$", "brand": "Brooks", "price": 74.99, "inventory": 1 },
    { "id": 8, "name": "GURESU 1.0 RUNNING SHOE", "image": "https://dsw.scene7.com/is/image/DSWShoes/399777_000_ss_01?$pdp-image$", "brand": "Reebok", "price": 74.99, "inventory": 2 },
    { "id": 9, "name": "SYREN GULF DUCK BOOT", "image": "https://dsw.scene7.com/is/image/DSWShoes/395850_710_ss_01?$pdp-image$", "brand": "Sperry Top-sider", "price": 99.99, "inventory": 6 },
    { "id": 10, "name": "BIT LOAFER", "image": "https://dsw.scene7.com/is/image/DSWShoes/408579_262_ss_01?$pdp-image$", "brand": "Mercanti Fiorentini", "price": 129.99, "inventory": 15 },
    { "id": 11, "name": "ADISSAGE SLIDE SANDAL", "image": "https://dsw.scene7.com/is/image/DSWShoes/173739_001_ss_01?$pdp-image$", "brand": "Adidas", "price": 29.99, "inventory": 11 },
    { "id": 12, "name": "DEVOTION PLUS 2 WALKING SHOE", "image": "https://dsw.scene7.com/is/image/DSWShoes/391789_101_ss_01?$pdp-image$", "brand": "Ryka", "price": 59.98, "inventory": 5 },
    { "id": 13, "name": "VOLT TODDLER & YOUTH SNEAKER", "image": "https://dsw.scene7.com/is/image/DSWShoes/361885_001_ss_01?$pdp-image$", "brand": "Saucony", "price": 44.99, "inventory": 9 }
];

export default data;