const brands = [
  { "id": 0, "name": "Nike" },
  { "id": 1, "name": "Van" },
  { "id": 2, "name": "Converse" },
  { "id": 3, "name": "Adidas" },
  { "id": 4, "name": "Reebok" },
  { "id": 5, "name": "New Balance" },
  { "id": 6, "name": "Brooks" },
  { "id": 7, "name": "Lacoste" },
  { "id": 8, "name": "Sperry Top-sider" },
  { "id": 9, "name": "Mercanti Fiorentini" },
  { "id": 10, "name": "ON" },
  { "id": 11, "name": "Ryka" },
  { "id": 12, "name": "Saucony" }
];

export default brands;
