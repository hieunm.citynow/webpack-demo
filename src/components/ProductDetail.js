import React from 'react';
import '../styles/styles.css';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'

class ProductDetail extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <div className="product-detail-holder">
                <div className="col-lg-6 col-md-6">
                    <img className="product-item-image" src={this.props.image} alt={this.props.name} />
                </div>
                <div className="col-lg-6 col-md-6">
                    <div className="detail-container">
                        <p className="num">Brand: </p>
                        <p className="product-item-name">{this.props.brand}</p>
                    </div>
                    <div className="detail-container">
                        <p className="num">Name: </p>
                        <p className="product-item-name">{this.props.name}</p>
                    </div>
                    <div className="detail-container">
                        <p className="num">Price: </p>
                        <p className="product-item-price">${(this.props.price * this.props.count).toFixed(2)}</p>
                    </div>
                    <div className="detail-container">
                        <p className="num">Quantity: </p>
                        <button 
                            className="btn btn-black"
                            onClick={this.props.onMinusClick}
                            ><i className="fa fa-minus icon-size"></i></button>
                        <p className="num"> {this.props.count} </p>
                        <button 
                            className="btn btn-black"
                            onClick={this.props.onPlusClick}
                            ><i className="fa fa-plus icon-size"></i></button>
                    </div>
                    <button
                        className="btn btn-black"
                        onClick={this.props.onAddToCartClicked}
                        disabled={this.props.inventory > 0 ? '' : 'disabled'}>
                        {this.props.inventory > 0 ? 'Add to cart' : 'Sold Out'}
                    </button>
                </div>  
            </div>
        )
    }
}

ProductDetail.propTypes = {
    onAddToCartClicked: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    inventory: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    //count: PropTypes.number.isRequired,
    brand: PropTypes.string,
    image: PropTypes.string,
}

ProductDetail.defaultProps = {
    image: "http://www.pixedelic.com/themes/geode/demo/wp-content/uploads/sites/4/2014/04/placeholder4.png",
    count: 1,
}

export default ProductDetail;