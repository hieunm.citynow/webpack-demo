import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../styles/styles.css';
import '../styles/App.css';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class Header extends Component {
  constructor(props) {
    super(props);
  }
// TODO: navbar toggle
  render() {
    return (
        <nav className="navbar App-header">
          <div className="container-fluid">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed color-button" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span> 
              </button>
              <Link to='/'>
                {/* <img className="App-logo" src="http://vuongquocgiaydep.com/kingdomshoesvn/uploads/2017/10/kingdomshoesnewblack.png" alt="Shoes Shop" /> */}
              </Link>
            </div>
            <div className="collapse navbar-collapse" id="myNavbar">
              <ul className="nav navbar-nav">
                <li><Link to="/" className="nav-custom">Home</Link></li>
                <li><Link to="/brands" className="nav-custom">Brands</Link></li>
              </ul>
              {/* <form class="form-inline my-2 my-lg-0 nav navbar-nav navbar-right"> */}
                <button type="button" className="btn btn-black btn-lg cart-btn navbar-btn navbar-right" data-toggle="modal" data-target="#myModal"><i className="fa fa-shopping-cart" aria-hidden="true"></i> <span className="badge badge-pill">{this.props.cart.length}</span></button>
              {/* </form> */}
            </div>
          </div>
        </nav>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.cart.cart,
  }
}

export default connect(mapStateToProps, null)(Header);
