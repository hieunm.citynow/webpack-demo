import React from 'react'
import CartItem from '../components/CartItem'
import PropTypes from 'prop-types'

class CartList extends React.Component{

    render() {
        return (
        this.props.cart.map((item, index) => (
            <CartItem key={index} image={item.product.image} name={item.product.name} price={item.product.price} count={item.count} 
                //onMinusClick={this.props.count} onPlusClick={this.props.count}
                onRemoveFromCartClicked={() => { this.props.onRemoveFromCartClicked(item); }}/>
        ))
    )};
}

CartList.propTypes = {
    cart: PropTypes.arrayOf(
        PropTypes.shape({
            product: PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                price: PropTypes.number.isRequired,
                image: PropTypes.string,
            }),
            count: PropTypes.number.isRequired,
        }).isRequired
    ).isRequired,
    onRemoveFromCartClicked: PropTypes.func.isRequired
}

CartList.defaultProps = {
}

export default CartList