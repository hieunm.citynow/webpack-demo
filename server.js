const path = require('path')
const express = require('express')
const port = process.env.PORT || 8080
// var http = require('http');

// //create a server object:
// http.createServer(function (req, res) {
//   res.writeHead(200, {'Content-Type': 'text/html'});
//   res.write('Hello World!'); //write a response to the client
//   res.end(); //end the response
// }).listen(8081); //the server object listens on port 8080

const app = express();
app.use(express.static(__dirname + '/dist/'));
app.get('*.js', function (req, res, next) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'text/javascript');
    next();
});

app.get('*.css', function (req, res, next) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'text/css');
    next();
});
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname + '/dist/', 'index.html'));
});
app.listen(port, () => console.log('Listening on port ' + port));